package com.demo.springboot.dto;

public class AlertDto {
    private String alert;

    public AlertDto(String alert) {
        this.alert = alert;
    }

    public AlertDto(){
    }

    public String getAlert() {
        return alert;
    }
}
