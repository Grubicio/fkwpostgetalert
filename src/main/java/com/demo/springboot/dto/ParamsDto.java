package com.demo.springboot.dto;

public class ParamsDto {

    private Double a;
    private Double b;
    private Double c;

    public ParamsDto() {
    }

    public Double getA() {
        return a;
    }

    public Double getB() {
        return b;
    }

    public Double getC() {
        return c;
    }

    @Override
    public String toString() {
        return "ParamsDto{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}


