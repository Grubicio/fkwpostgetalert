package com.demo.springboot.rest;

import com.demo.springboot.dto.AlertDto;
import com.demo.springboot.dto.ParamsDto;
import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.service.QuadraticFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DocumentApiController {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentApiController.class);

    private final QuadraticFunction quadraticFunction;


    public DocumentApiController(QuadraticFunction quadraticFunction) {
        this.quadraticFunction = quadraticFunction;
    }

    @GetMapping("/api/math/quadratic-function")
    public ResponseEntity<?> calculateFunction(@RequestParam(value = "a", required = false) Double a,
                                               @RequestParam(value = "b", required = false) Double b,
                                               @RequestParam(value = "c", required = false) Double c) {


        if(a == null){
            return new ResponseEntity<>(new AlertDto("Brak parametru a"), HttpStatus.BAD_REQUEST);
        }else if(b == null){
            return new ResponseEntity<>(new AlertDto("Brak parametru b"), HttpStatus.BAD_REQUEST);
        }else if(c == null){
            return new ResponseEntity<>(new AlertDto("Brak parametru c"), HttpStatus.BAD_REQUEST);
        }

        LOG.info("---- a: {}", a);
        LOG.info("---- b: {}", b);
        LOG.info("---- c: {}", c);

        return ResponseEntity.ok(quadraticFunction.calculateFunction(a, b, c));
    }

    @PostMapping("/api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateFunctionAsPost(@RequestBody ParamsDto paramsDto) {

        LOG.info("---- Params: {}", paramsDto.toString());

        return ResponseEntity
                .ok(quadraticFunction.calculateFunction(paramsDto.getA(), paramsDto.getB(), paramsDto.getC()));
    }

}
